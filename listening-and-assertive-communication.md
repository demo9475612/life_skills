## Active Listening
### Question 1: What are the steps/strategies to do Active Listening? (Minimum 6 points)
- **Pay Attention:** Focus on the speaker, make eye contact, and eliminate distractions.
- **Show That You're Listening:** Nod, smile, and use verbal cues like "yes," "I see," or "go on."
-**Provide Feedback:** Summarize what you've heard, ask clarifying questions, and empathize with the speaker.
- **Defer Judgment:** Avoid interrupting or jumping to conclusions. Keep an open mind.
-**Respond Appropriately:** Wait for the speaker to finish before responding. Tailor your response to what they've said.
- **Be Mindful of Nonverbal Cues:** Pay attention to body language, tone of voice, and other nonverbal signals.

## Reflective Listening
### Question 2: According to Fisher's model, what are the key points of Reflective Listening?

Reflective listening, according to Fisher's model, emphasizes mirroring body language, emotions, and verifying the message. It involves:

- **Mirroring Body Language:** Paying attention to the speaker's gestures, posture, and facial expressions.
- ***Mirroring Emotions:** Reflecting the speaker's feelings to show empathy and understanding.
- **Verifying the Message:** Confirming understanding by paraphrasing or summarizing what the speaker has said.

## Reflection
### Question 3: What are the obstacles in your listening process?

Some common obstacles in the listening process include distractions, preconceived notions, personal biases, lack of empathy, and emotional barriers.

### Question 4: What can you do to improve your listening?

To improve listening skills, one can practice active listening techniques, engage in mindfulness exercises, seek feedback from others, practice empathy, and cultivate a genuine interest in understanding others.

## Types of Communication
### Question 5: When do you switch to Passive communication style in your day-to-day life?

I may switch to a passive communication style when I feel intimidated, lack confidence, or want to avoid conflict. It can happen in situations where I don't feel comfortable expressing my opinions or asserting myself.

### Question 6: When do you switch into Aggressive communication styles in your day-to-day life?

I might switch to an aggressive communication style when I feel threatened, frustrated, or perceive a need to dominate a conversation. It can occur in heated arguments or when I feel my boundaries are being violated.

### Question 7: When do you switch into Passive Aggressive communication styles in your day-to-day life?

Passive-aggressive communication styles, such as sarcasm, gossiping, taunts, or giving the silent treatment, may arise when I want to express my frustrations indirectly or avoid direct confrontation.

### Question 8: How can you make your communication assertive?

 **To make communication assertive, I can:** 
Practice expressing my thoughts and feelings clearly and directly.
Use "I" statements to express my needs and boundaries without blaming others.  
Maintain a confident and calm demeanor, even in challenging situations.
Listen actively and respectfully to others' perspectives.   
Practice assertive body language, such as maintaining eye contact and using confident posture.     
Set clear and respectful boundaries when necessary.