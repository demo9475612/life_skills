### Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
Answer: The video discusses grit, emphasizing the importance of perseverance and passion for long-term goals, even in the face of adversity. It highlights how grit, more than talent alone, leads to success.

### Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
Answer: The video introduces the concept of growth mindset, contrasting it with a fixed mindset. It emphasizes the belief that abilities can be developed through dedication and hard work, rather than being fixed traits.

### Question 3: What is the Internal Locus of Control? What is the key point in the video?
Answer: The internal locus of control refers to the belief that one has control over their own actions and outcomes. The key point in the video is that individuals with an internal locus of control tend to be more motivated and achieve greater success because they believe they can influence their circumstances.

### Question 4: What are the key points mentioned by the speaker to build a growth mindset (explanation not needed)?

Answer: The key points mentioned to build a growth mindset include embracing challenges, persisting in effort, learning from feedback, inspiring others, cultivating curiosity, celebrating progress, staying resilient, setting goals, seeking learning opportunities, and reflecting and adjusting.

### Question 5: What are your ideas to take action and build Growth Mindset?
Answer: Points to build Growth Mindset are:
- Instead of avoiding challenges, actively seek them out. View them as opportunities to learn and grow, rather than as threats.
- Understand that mastery comes with consistent effort over time. When faced with obstacles or setbacks, persevere and keep pushing forward.
- Welcome feedback, both positive and constructive criticism. Use it as an opportunity to improve and refine your skills.
- Stay curious and open-minded. Explore new ideas, technologies, and approaches. Ask questions and seek out answers.
- Acknowledge and celebrate your progress, no matter how small. Recognize that every step forward is a step toward improvement.