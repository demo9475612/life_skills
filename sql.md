# Understanding SQL: A Comprehensive Guide 

## Abstract
This paper aims to provide a comprehensive understanding of Structured Query Language (SQL) by covering its basics, joins, and aggregations. It includes code samples and explanations to facilitate learning for both beginners and experienced practitioners.

## 1. Introduction to SQL
Structured Query Language (SQL) is a standard programming language for managing relational databases. It provides powerful tools for querying and manipulating data.
## 2. Basics of SQL
SQL includes fundamental operations for interacting with databases. Here are some basic concepts:

### Creating a Table

```bash
 CREATE TABLE Employees (
    EmployeeID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    DepartmentID INT
);
```


### Inserting Data

```bash
INSERT INTO Employees (EmployeeID, FirstName, LastName, DepartmentID)
VALUES (1, 'John', 'Doe', 101),
       (2, 'Jane', 'Smith', 102),
       (3, 'Mike', 'Johnson', 101);

```
### Selecting Data

```bash
SELECT * FROM Employees;

```
## 3. Joins in SQL
Joins are used to combine data from multiple tables based on a related column. Here are some common join types:
### Types of Join

 - Inner Join
 - Left Join
 - Right Join
 - Cross Join

## 4. Aggregations in SQL
Aggregations are used to perform calculations on data. Here are some common aggregation functions:

### Count

```bash
SELECT COUNT(*) AS TotalEmployees
FROM Employees;
```

### Sum

```bash
SELECT SUM(Salary) AS TotalSalary
FROM Employees;
```

### Average

```bash
SELECT AVG(Salary) AS AverageSalary
FROM Employees;
```

## References

* [MySQL Tutorial](https://www.w3schools.com/MySQL/default.asp)
* [MySQL Joins](https://www.javatpoint.com/mysql-join)
* [SQL Aggregate functions](https://www.geeksforgeeks.org/aggregate-functions-in-sql/)