### What kinds of behavior cause sexual harassment?
Sexual harassment happens when someone does things like touching you when you don't want it, saying sexual stuff you don't like, asking you for sexual things, making jokes or comments that make you uncomfortable, showing you inappropriate pictures or videos, or making your workplace feel bad because of your gender.

### What would you do if you face or see any incident of such behavior?
If someone does these things to you or someone else, you should:
- Tell someone in like your boss or HR.
- Write down what happened, including when and where.
- Talk to someone you trust for support.
- Follow your company's rules for reporting and dealing with sexual harassment.
- If needed, get advice from a lawyer.