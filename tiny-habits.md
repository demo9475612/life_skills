# Tiny Habits
### Question 1
In this video, what was the most interesting story or idea for you?
### Answer 1
The story that captured my interest the most was BJ Fogg's personal journey using tiny habits to modify his behavior. He initiated this transformation by incorporating a mere two push-ups after using the restroom, gradually escalating the count until he reached a daily routine of 50-60 push-ups.
### Question 2
How can you use B = MAP to make making new habits easier? What are M, A and P.
### Answer 2
The Tiny Habits approach employs the B = MAP formula, simplifying the formation of new habits through three steps:
- Reducing the behavior's scale
- Identifying an action prompt
- Celebrating achievements

B = MAP represents Behavior = Motivation + Ability + Prompt.
M signifies Motivation, reflecting one's desire or willingness to act.
A represents Ability, indicating one's capability to perform a task.
P stands for Prompt, acting as a trigger that prompts action.
### Question 3
Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
### Answer 3
Celebrating holds significance for several reasons:
- Firstly, it establishes a positive connection with the habit. By celebrating post-engagement, you affirm your success and cultivate pride, creating a positive inclination to repeat the habit.
- Secondly, celebrations enhance motivation. Positive emotions following habit completion boost the likelihood of repeating it, as motivation often stems from positive feelings.
### Question 4
In this video, what was the most interesting story or idea for you?
### Answer 4
The narrative of James Clear and his writing habit stands out, illustrating how his consistent practice led to the creation of over 400 articles and the growth of his website to over a million monthly visitors.
### Question 5
What is the book's perspective about Identity?
### Answer 5
The book argues that our identity is shaped by a collection of tiny habits performed daily. Regardless of their size, these habits mold our present and future selves. The book provides a framework for habit development and breaking, advocating for small, incremental changes. It underscores the importance of celebrating even minor successes, emphasizing their role in building momentum and motivation.
### Question 6
Write about the book's perspective on how to make a habit easier to do?
### Answer 6
The book also imparts practical advice for building positive habits and discarding negative ones:
- Initiate changes gradually, focusing on manageable adjustments.
- Enhance visibility for positive habits and minimize cues for negative ones.
- Associate positive habits with enjoyable activities.
- Simplify the execution of positive habits to reduce friction.
- Provide immediate rewards for positive habit adherence.
### Question 7
Write about the book's perspective on how to make a habit harder to do?
### Answer 7
To make a habit more challenging, the book suggests:
- Eliminating triggers for the habit.
- Diminishing the habit's enjoyment.
- Increasing the difficulty of habit execution.
- Removing factors that make the habit rewarding.
### Question 8:
Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
### Answer 8
I aspire to read more books. Steps to make this habit more appealing or manageable include:
- Setting a daily reminder for dedicated reading time.
- Creating a cozy reading corner at home.
- Starting with shorter reading sessions and progressively extending them.
- Rewarding myself with a small treat upon finishing a book.
### Question 9:
Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
### Answer 9
The habit I aim to eliminate is mindless slacking while studying. Strategies to make this habit less attractive or more challenging involve:
- Designating a distraction-free study area.
- Turning off unnecessary electronic devices or implementing website blockers.
- Structuring study sessions with focused intervals and planned breaks.
