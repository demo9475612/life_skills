### Question 1
The point about using tools like TimeLimit or Freedom to block social media sites and apps during work hours was new to me. I hadn't considered using such tools before to improve productivity.

### Question 2
I think I need to improve on over-communication and being aware of other team members' schedules. One idea to make progress in this area is to set reminders to check in with team members regularly, ensuring that I'm available for any updates or questions they may have. Additionally, I could make a conscious effort to streamline communication by organizing my questions or updates before reaching out, minimizing unnecessary interruptions to their workflow.